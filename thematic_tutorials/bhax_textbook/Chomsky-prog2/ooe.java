package com.epam.training;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class OrderOfEverythingTest {

    @Test(dataProvider = "collectionsToSortDataProvider")
    public void testOrderShouldReturnExpectedListWhenCollectionIsPassed(Collection<Integer> input, List<Integer> expectedOutput) {
        List<Integer> actualOutput = createOrderedList(input);
        assertThat(actualOutput, equalTo(expectedOutput));
    }

    @DataProvider
    private Object[][] collectionsToSortDataProvider() {
        return new Object[][] {
            {Collections.emptySet(), Collections.emptyList()},
            {Set.of(1), List.of(1)},
            {Set.of(2,1), List.of(1,2)}
        };
    }

    private <T extends Comparable<T>> List<T> createOrderedList(Collection<T> input) {
        return input.stream()
            .sorted()
            .collect(Collectors.toList());
    }
}
© 202
