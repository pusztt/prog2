﻿from __future__ import print_function
# ------------------------------------------------------------------------------------------------
# Copyright (c) 2016 Microsoft Corporation
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
# NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ------------------------------------------------------------------------------------------------

# Tutorial sample #2: Run simple mission using raw XML

# Added modifications by Norbert Bátfai (nb4tf4i) batfai.norbert@inf.unideb.hu, mine.ly/nb4tf4i.1
# 2018.10.18, https://bhaxor.blog.hu/2018/10/18/malmo_minecraft
# 2020.02.02, NB4tf4i's Red Flowers, http://smartcity.inf.unideb.hu/~norbi/NB4tf4iRedFlowerHell


from builtins import range
import MalmoPython
import os
import sys
import time
import random
import json
import math

if sys.version_info[0] == 2:
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)  # flush print output immediately
else:
    import functools
    print = functools.partial(print, flush=True)

# Create default Malmo objects:

agent_host = MalmoPython.AgentHost()
try:
    agent_host.parse( sys.argv )
except RuntimeError as e:
    print('ERROR:',e)
    print(agent_host.getUsage())
    exit(1)
if agent_host.receivedArgument("help"):
    print(agent_host.getUsage())
    exit(0)

# -- set up the mission -- #
missionXML_file='nb4tf4i_d.xml'
with open(missionXML_file, 'r') as f:
    print("NB4tf4i's Red Flowers (Red Flower Hell) - DEAC-Hackers Battle Royale Arena\n")
    print("NB4tf4i vörös pipacsai (Vörös Pipacs Pokol) - DEAC-Hackers Battle Royale Arena\n\n")
    print("The aim of this first challenge, called nb4tf4i's red flowers, is to collect as many red flowers as possible before the lava flows down the hillside.\n")
    print("Ennek az első, az nb4tf4i vörös virágai nevű kihívásnak a célja összegyűjteni annyi piros virágot, amennyit csak lehet, mielőtt a láva lefolyik a hegyoldalon.\n")    
    print("Norbert Bátfai, batfai.norbert@inf.unideb.hu, https://arato.inf.unideb.hu/batfai.norbert/\n\n")    
    print("Loading mission from %s" % missionXML_file)
    mission_xml = f.read()
    my_mission = MalmoPython.MissionSpec(mission_xml, True)
    my_mission.drawBlock( 0, 0, 0, "lava")


class Hourglass:
    def __init__(self, charSet):
        self.charSet = charSet
        self.index = 0
    def cursor(self):
        self.index=(self.index+1)%len(self.charSet)
        return self.charSet[self.index]

hg = Hourglass('|/-\|')

class Steve:
    def __init__(self, agent_host):
        self.agent_host = agent_host
        
        self.nof_red_flower = 0

    def run(self):
        world_state = self.agent_host.getWorldState()
        # Loop until mission ends:
        self.agent_host.sendCommand("look 1")
        self.agent_host.sendCommand("look 1")
        self.agent_host.sendCommand("tp 116.5 59 87.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(.5)
        self.agent_host.sendCommand("tp -8.5 60 -117.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(.5)
        self.agent_host.sendCommand("tp -41.5 58 114.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -111.5 57 -88.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 31.5 56 -109.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -16.5 55 108.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -105.5 54 93.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -85.5 53 104.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 48.5 52 -101.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 18.5 51 100.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 15.5 50 98.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 57.5 49 96.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -38.5 48 -93.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 3.5 47 92.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -89.5 46 72.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -31.5 45 -87.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -62.5 44 86.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 4.59 43 84.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -14.5 42 82.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 80.5 41 0.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 78.5 40 -9.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 3.5 39 76.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 74.5 38 5.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -63.5 37 -71.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -48.5 36 -69.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 47.5 35 68.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 66.5 34 -62.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 23.5 33 -63.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -61.5 32 -14.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 44.5 31 60.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 58.5 30 -10.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 39.5 29 56.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -7.5 28 -53.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 52.5 27 42.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -26.5 26 50.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 30.5 25 -47.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -27.5 24 46.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -42.5 23 -43.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 32.5 22 -41.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 8.5 21 -39.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 38.5 20 -0.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -34.5 19 36.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 34.5 18 24.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 17.5 17 -31.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 14.5 16 30.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 28.5 15 24.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 26.5 14 -14.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 4.5 13 -23.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -21.5 12 -21.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 4.5 11 20.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -17.5 10 -12.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -8.5 9 16.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 1.5 8 14.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -10.5 7 12.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -5.5 6 -9.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp -2.5 5 8.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 6.5 4 4.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(1.5)
        self.agent_host.sendCommand("tp 2.5 3 4.5")
        time.sleep(.5)
        self.agent_host.sendCommand("attack 1")
        time.sleep(.5)

num_repeats = 1
for ii in range(num_repeats):

    my_mission_record = MalmoPython.MissionRecordSpec()

    # Attempt to start a mission:
    max_retries = 6
    for retry in range(max_retries):
        try:
            agent_host.startMission( my_mission, my_mission_record )
            break
        except RuntimeError as e:
            if retry == max_retries - 1:
                print("Error starting mission:", e)
                exit(1)
            else:
                print("Attempting to start the mission:")
                time.sleep(2)

    # Loop until mission starts:
    print("   Waiting for the mission to start ")
    world_state = agent_host.getWorldState()

    while not world_state.has_mission_begun:
        print("\r"+hg.cursor(), end="")
        time.sleep(0.15)
        world_state = agent_host.getWorldState()
        for error in world_state.errors:
            print("Error:",error.text)

    print("NB4tf4i Red Flower Hell running\n")
    steve = Steve(agent_host)
    steve.run()
    print("Number of flowers: "+ str(steve.nof_red_flower))

print("Mission ended")
# Mission has ended.
